const map = [
    'WWWWWWWWWWWWWWWWWWWWW',
    'S   W     W     W W W',
    'W W W WWW WWWWW W W W',
    'W W W   W     W W   W',
    'W WWWWWWWWW WWW W W W',
    'W         W     W W W',
    'W WWW WWWWW WWWWW W W',
    'W  W  W   W W     W W',
    'WW W WW W WWW WWW W W',
    'W  W  W W W W W W W W',
    'WWWWW W W W W W WWW W',
    'W     W W W   W W   W',
    'W WWWWW W WW WW W WWW',
    'W       W       W   F',
    'WWWWWWWWWWWWWWWWWWWWW',
]

for (paredeL in map) {
    let parede_linha = document.createElement('div')
    document.body.appendChild(parede_linha).className = 'linha'
    linhas = map[paredeL]

    for (paredeC in linhas) {
        let parede_coluna = document.createElement('div')
        parede_coluna.setAttribute('paredeL', paredeL)
        parede_coluna.setAttribute('paredeC', paredeC)

        if (linhas[paredeC] === 'W') {
            parede_coluna.className = 'parede'
        } else if (linhas[paredeC] === 'S') {
            parede_coluna.className = 'largada'
        } else if (linhas[paredeC] === 'F') {
            parede_coluna.className = 'chegada'
        } else {
            parede_coluna.className = 'caminho'
        }
        parede_linha.appendChild(parede_coluna)
    }
}

let largada = document.getElementsByClassName('largada')
let linha_largada = largada[0].getAttribute('paredeL')
let coluna_largada = largada[0].getAttribute('paredeC')
let jogador = document.createElement('img')
jogador.src = './imagens/sonic.png'
jogador.setAttribute('paredeL',linha_largada)
jogador.setAttribute('paredeC',coluna_largada)
largada[0].appendChild(jogador)

document.addEventListener('keydown', (event) => {
    let keyName = event.key

    let jogadorL = Number(jogador.getAttribute('paredeL'))
    let jogadorC = Number(jogador.getAttribute('paredeC'))
    
    if (keyName === 'ArrowUp') {
        jogadorL -= 1
        let caminho = document.getElementsByClassName('caminho')
        for (let passo in caminho) {
            if (parseInt(caminho[passo].getAttribute('paredeL')) === jogadorL && parseInt(caminho[passo].getAttribute('paredeC')) === jogadorC) {
                jogador.setAttribute('paredeL', jogadorL)
                caminho[passo].appendChild(jogador)
            }
        }
    } else if (keyName === 'ArrowDown') {
        jogadorL += 1
        let caminho = document.getElementsByClassName('caminho')
        for (let passo in caminho) {
            if (parseInt(caminho[passo].getAttribute('paredeL')) === jogadorL && parseInt(caminho[passo].getAttribute('paredeC')) === jogadorC) {
                jogador.setAttribute('paredeL',jogadorL)
                caminho[passo].appendChild(jogador)
            }
        }
    } else if (keyName === 'ArrowRight') {
        if (jogadorL === 13 && jogadorC === 19) {
            vitoria()
            window.addEventListener('keydown', function (e) {
                e.stopPropagation()
            }, true)
        }
        jogadorC += 1
        let caminho = document.getElementsByClassName('caminho')
        for (let passo in caminho) {
            if (parseInt(caminho[passo].getAttribute('paredeL')) === jogadorL && parseInt(caminho[passo].getAttribute('paredeC')) === jogadorC) {
                jogador.setAttribute('paredeC',jogadorC)
                caminho[passo].appendChild(jogador)
            }
        }
    } else if (keyName === 'ArrowLeft') {
        jogadorC -= 1
        let caminho = document.getElementsByClassName('caminho')
        for (let passo in caminho) {
            if (parseInt(caminho[passo].getAttribute('paredeL')) === jogadorL && parseInt(caminho[passo].getAttribute('paredeC')) === jogadorC) {
                jogador.setAttribute('paredeC',jogadorC)
                caminho[passo].appendChild(jogador)
            }
        }
    }
})

function vitoria(){
    let vitoria = document.createElement('img')
    vitoria.src = './imagens/sonic-amy.gif'
    vitoria.className = 'vitoria'
    document.getElementById('vitoria').appendChild(vitoria)
    let chegada = document.getElementsByClassName('chegada')
    chegada[0].appendChild(jogador)
    setTimeout(() => {
        document.location.reload(true)
    }, 6000)
}
